#include "systemuser.h"

QList<SystemUser> SystemUser::deserializeList(const QByteArray &data)
{
    QList<SystemUser> users;

    QJsonDocument document = QJsonDocument::fromJson(data);
    if  (document.isArray())
    {
        QJsonArray usersArray = document.array();
        foreach (const QJsonValue & v, usersArray)
        {
            SystemUser user(v.toObject());
            users << user;
        }
    }
    return users;
}

QByteArray SystemUser::serializeList(const QList<SystemUser> &users)
{
    QJsonArray systemUserListJson;
    foreach (auto user, users) {

        systemUserListJson.append(formJson(user));
    }
    QJsonDocument document(systemUserListJson);

    return document.toJson();
}

QMap<int, QString> SystemUser::mPermissionNames = SystemUser::fillPermissionNames();

QMap<int, QString> SystemUser::fillPermissionNames() {
    QMap<int,QString> map;
    map.insert(SystemUser::LoginToSystem, "Вход в программу");
    map.insert(SystemUser::EditSystemUsers, "Редактирование учетных записей");
    return map;
}

QMap<int, QString> SystemUser::permissionNames()
{
    return mPermissionNames;
}

void SystemUser::setUuid(const QUuid &uuid)
{
    mUuid = uuid;
}

QUuid SystemUser::uuid() const
{
    return mUuid;
}

bool SystemUser::getIsConnected() const
{
    return isConnected;
}

void SystemUser::setIsConnected(bool value)
{
    isConnected = value;
}

QJsonObject SystemUser::formJson(SystemUser user)
{
    QJsonObject jsonUser;
    jsonUser["Uuid"] = user.uuid().toString();
    jsonUser["Name"] = user.name();
    jsonUser["Login"] = user.login();
    jsonUser["PassHash"] = user.passHash();
    jsonUser["IsConnected"] = user.getIsConnected();

    QJsonArray permissions;
    auto userPermissions = user.permissions();
    foreach (auto permission, userPermissions) {
        permissions.append(permission);
    }
    jsonUser["Permissions"] = permissions;

    return jsonUser;
}

SystemUser::SystemUser()
{

}

SystemUser::SystemUser(const QString &login, const QString &passHash)
{
    mLogin = login;
    mPassHash = passHash;
}

SystemUser::SystemUser(const QString &login, const QString &passHash, const QList<int> &permissions)
{
    mLogin = login;
    mPassHash = passHash;
    mPermissions.clear();
    mPermissions.append(permissions);}

SystemUser::SystemUser(const QString &login, const QString &name, const QString &passHash, const QList<int> &permissions)
{
    mLogin = login;
    mName = name;
    mPassHash = passHash;
    mPermissions.clear();
    mPermissions.append(permissions);
}

SystemUser::SystemUser(const QUuid &uuid, const QString &login, const QString &name, const QString &passHash, const QList<int> &permissions)
{
    mUuid = uuid;
    mLogin = login;
    mName = name;
    mPassHash = passHash;
    mPermissions.clear();
    mPermissions.append(permissions);
}

SystemUser::SystemUser(const QByteArray &serverData)
{
    QJsonDocument document = QJsonDocument::fromJson(serverData);
    if  (document.isObject())
    {
        deserialize(document.object());
    }
}

SystemUser::SystemUser(QJsonObject serverData)
{

    deserialize(serverData);
}

QString SystemUser::login() const
{
    return mLogin;
}

QList<int> SystemUser::permissions() const
{
    return mPermissions;
}

QString SystemUser::name() const
{
    return mName;
}

QString SystemUser::passHash() const
{
    return mPassHash;
}

QByteArray SystemUser::serialize()
{
    QJsonDocument document(formJson(this));
    return document.toJson();
}



void SystemUser::deserialize(const QJsonObject &obj)
{

    mUuid = QUuid(obj.value("Uuid").toString());
    mLogin = obj.value("Login").toString();
    mName = obj.value("Name").toString();
    mPassHash = obj.value("PassHash").toString();
    isConnected = obj.value("IsConnected").toBool();

    QJsonArray jsonPermissions = obj.value("Permissions").toArray();
    foreach (auto permission, jsonPermissions) {
        mPermissions.append(permission.toInt());
    };


}



void SystemUser::setLogin(const QString &login)
{
    mLogin = login;
}

void SystemUser::setName(const QString &name)
{
    mName = name;
}

void SystemUser::addPermission(int permission)
{
    mPermissions.append(permission);
}

void SystemUser::clearPermissions()
{
    mPermissions.clear();
}

void SystemUser::setPermissions(const QList<int> &permissions)
{
    mPermissions.clear();
    mPermissions.append(permissions);
}



