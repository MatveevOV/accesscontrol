#ifndef SYSTEMUSER_H
#define SYSTEMUSER_H

#include <QObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QMap>
#include <QUuid>

#include <QDebug>


class SystemUser
{
public:
    SystemUser();
    SystemUser(const QString &login, const QString &passHash);
    SystemUser(const QString &login, const QString &passHash, const QList<int> &permissions);
    SystemUser(const QString &login, const QString &name, const QString &passHash, const QList<int> &permissions);
    SystemUser(const QUuid &uuid, const QString &login, const QString &name, const QString &passHash, const QList<int> &permissions);

    SystemUser(const QByteArray &serverData);
    SystemUser(QJsonObject serverData);

    enum SystemUserPermissions
    {
        LoginToSystem = 1,
        EditSystemUsers = 2
    };

    enum UserOperationResult
    {
      UserAdded,
      UserEdited,
      UserRemoved,
      UserIsDuplicate,
      UserNotFound,
      UnknownError
    };

    QString login() const;
    QList<int> permissions() const;
    QString name() const;
    QString passHash() const;
    QByteArray serialize();

    void setLogin(const QString &login);
    void setName(const QString &name);
    void addPermission(int permission);
    void clearPermissions();
    void setPermissions(const QList<int> &permissions);

    static QList<SystemUser> deserializeList(const QByteArray &data);
    static QByteArray serializeList(const QList<SystemUser> &users);
    static QMap<int, QString> permissionNames();

    void setUuid(const QUuid &uuid);
    QUuid uuid() const;

    bool getIsConnected() const;
    void setIsConnected(bool value);

private:
    QUuid mUuid;
    QString mLogin;
    QString mPassHash;
    QString mName;
    QList<int> mPermissions;
    static QMap<int, QString> mPermissionNames;
    bool isConnected = false;

    QJsonObject formJson(SystemUser user);
    void deserialize(const QJsonObject &obj);
    static QMap<int, QString> fillPermissionNames();


};

#endif // SYSTEMUSER_H
