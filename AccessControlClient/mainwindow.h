#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStandardItem>
#include <QStandardItemModel>
#include <QDebug>
#include <QMessageBox>
#include <QAbstractSocket>

#include "../Shared/systemuser.h"
#include "systemuserprocessor.h"
#include "systemusereditdialog.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
public slots:
    void openWindow(const SystemUser &user);
    void setSystemUserProcessorLink(SystemUserProcessor * systemUserProcessor);
    void handleSystemUserOperationResult(int data);    
    void serverErrorProcessing(QAbstractSocket::SocketError socketError);

private slots:
    void fillCurrentUserInfo(const SystemUser &user);
    void on_systemUserTabButton_released();
    void on_addSystemUserButton_released();
    void on_editSystemUserButton_released();
    void on_removeSystemUserButton_released();
    void disableTabButtons();
    void checkUserPermissions(const SystemUser &user);


    void on_devicesTabButton_clicked();

signals:
    void needSystemUserList();

private:
    Ui::MainWindow *ui;
    SystemUser currentUser;
    QStandardItemModel systemUserModel;
    SystemUserProcessor *systemUserProcessor;

};

#endif // MAINWINDOW_H
