#ifndef SYSTEMUSERPROCESSOR_H
#define SYSTEMUSERPROCESSOR_H

#include <QObject>
#include <QStandardItem>
#include <QStandardItemModel>
#include <QUuid>
#include <QDebug>

#include "systemuser.h"

class SystemUserProcessor : public QObject
{
    Q_OBJECT

public:
    SystemUserProcessor();    
    QStandardItemModel *getModel() const;
    void addSystemUser(const SystemUser &user);
    void editSystemUser(const SystemUser &user);
    void removeSystemUser(const SystemUser &user);


    void addSystemUserInModel();
    void editSystemUserInModel();
    void removeSystemUserInModel();

    void setSelectedUserIndex(const QModelIndex &value);
    SystemUser getSelectedSystemUser(const QModelIndex &value);

private:
    QStandardItemModel *model;
    QModelIndex selectedUserIndex;
    QHash<QUuid, SystemUser> users;
    SystemUser newUser;

public slots:
    void fillModel(const QByteArray &usersJson);

signals:
    tryAddSystemUser(const SystemUser &user);
    tryEditSystemUser(const SystemUser &user);
    tryRemoveSystemUser(const SystemUser &user);
};

#endif // SYSTEMUSERPROCESSOR_H
