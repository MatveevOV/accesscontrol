#include "systemusereditdialog.h"
#include "ui_systemusereditdialog.h"

SystemUserEditDialog::SystemUserEditDialog(SystemUser user, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SystemUserEditDialog)
{
    ui->setupUi(this);

    editedUser = user;

    composePermissionWidget();

    if (!user.uuid().isNull())
    {
        ui->resetPassCheckBox->setChecked(false);
    }

    ui->passEdit->setEnabled(ui->resetPassCheckBox->isChecked());
    ui->secondPassEdit->setEnabled(ui->resetPassCheckBox->isChecked());

    connect(ui->resetPassCheckBox, &QCheckBox::toggled, ui->passEdit, &QLineEdit::setEnabled);
    connect(ui->resetPassCheckBox, &QCheckBox::toggled, ui->secondPassEdit, &QLineEdit::setEnabled);

    ui->passAttention->setVisible(false);

    connect(ui->passEdit, &QLineEdit::textEdited, this, &SystemUserEditDialog::checkPass);
    connect(ui->secondPassEdit, &QLineEdit::textEdited, this, &SystemUserEditDialog::checkPass);



    ui->loginEdit->setText(editedUser.login());
    ui->nameEdit->setText(editedUser.name());


}

SystemUserEditDialog::~SystemUserEditDialog()
{
    delete ui;
}

void SystemUserEditDialog::composePermissionWidget()
{
    QVBoxLayout *permissionLayout = new QVBoxLayout;
    QLabel *l = new QLabel("Права доступа");
    permissionLayout->addWidget(l);
    QMap<int, QString> permissionNames = SystemUser::permissionNames();
    auto iter = permissionNames.begin();
    auto editedUserPermissions = editedUser.permissions();
    for (; iter != permissionNames.end(); ++iter)
    {
        QCheckBox *permissionCheckBox = new QCheckBox(iter.value());
        permissionCheckBox->setObjectName(QString::number(iter.key()));
        permissionCheckBox->setChecked(editedUserPermissions.contains(iter.key()));
        permissionLayout->addWidget(permissionCheckBox);
    }

    permissionLayout->addStretch(1);
    ui->permissionWidget->setLayout(permissionLayout);
}

void SystemUserEditDialog::checkPass()
{
    bool passMatch = ui->passEdit->text() == ui->secondPassEdit->text();
    ui->passAttention->setVisible(!passMatch);
    ui->okButton->setEnabled(passMatch);
}



void SystemUserEditDialog::on_cancelButton_released()
{
    reject();
}

void SystemUserEditDialog::on_okButton_released()
{

    if (ui->resetPassCheckBox->isChecked())
    {
        QByteArray passHash = QCryptographicHash::hash(ui->passEdit->text().toUtf8(),QCryptographicHash::Sha256);
        SystemUser user(ui->loginEdit->text(), ui->nameEdit->text(), QString(passHash.toHex()), formPermissionList());

        emit sendUser(user);
    }
    else
    {
        SystemUser user;
        user.setName(ui->nameEdit->text());
        user.setLogin(ui->loginEdit->text());
        user.setPermissions(formPermissionList());
        emit sendUser(user);
    }

    accept();
}

QList<int> SystemUserEditDialog::formPermissionList()
{
    QList<int> permissions;
    QList<QCheckBox *> permissionCheckBoxes = ui->permissionWidget->findChildren<QCheckBox*>();
    foreach (auto checkBox, permissionCheckBoxes) {
        if (checkBox->isChecked())
            permissions.append(checkBox->objectName().toInt());
    }
    return permissions;
}
