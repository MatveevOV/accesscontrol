#include <QApplication>
#include <QThread>

#include "logindialog.h"
#include "mainwindow.h"
#include "tcpsocket.h"
#include "systemuserprocessor.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    LoginDialog loginDialog;
    MainWindow mainWindow;

    TcpSocket socket;
    socket.createSocket();
    SystemUserProcessor systemUserProcessor;


//    QThread *socketThread = new QThread;
//    QObject::connect(&a, &QApplication::destroyed, socketThread, &QThread::quit); //при разрушении основного окна завершить поток

//    QObject::connect(socketThread, &QThread::started,
//            &socket, &TcpSocket::createSocket);//THREAD connect
//    QObject::connect(socketThread, &QThread::finished, &socket, &TcpSocket::deleteLater);//Socket ENd
//    QObject::connect(socketThread, &QThread::finished, socketThread, &QThread::deleteLater);//THREAD ENd


    QObject::connect(&loginDialog, &LoginDialog::sendConnectionSettings, &socket, &TcpSocket::connectToServer, Qt::DirectConnection);
    QObject::connect(&loginDialog, &LoginDialog::sendAuthorizationSettings, &socket, &TcpSocket::sendAuthorizationSettings);

    QObject::connect(&socket, &QAbstractSocket::connected, &loginDialog, &LoginDialog::tryToAuthorize);
    QObject::connect(&socket, &TcpSocket::systemUserAuthorized, &loginDialog, &LoginDialog::allowUserAccess);
    QObject::connect(&socket, &TcpSocket::systemUserUnauthorized, &loginDialog, &LoginDialog::denyUserAccess);
    QObject::connect(&socket, &TcpSocket::systemUserAlreadyConnected, &loginDialog, &LoginDialog::denyUserAccess);


    QObject::connect(&loginDialog, &LoginDialog::sendDisconnect, &socket, &TcpSocket::disconnectFromServer);

    QObject::connect(&socket,static_cast<void(QAbstractSocket::*)(QAbstractSocket::SocketError)>(&QAbstractSocket::error),
                     &mainWindow, &MainWindow::close);

    QObject::connect(&socket,static_cast<void(QAbstractSocket::*)(QAbstractSocket::SocketError)>(&QAbstractSocket::error),
                     &loginDialog, &LoginDialog::serverErrorProcessing);



    QObject::connect(&socket,&TcpSocket::disconnectDueServer, &mainWindow, &MainWindow::close);
    QObject::connect(&socket,&TcpSocket::disconnectDueServer, &loginDialog, &LoginDialog::showAfterDisconnect);

    QObject::connect(&loginDialog, &LoginDialog::needShowMainWindow, &mainWindow, &MainWindow::openWindow);
    QObject::connect(&mainWindow, &MainWindow::needSystemUserList, &socket, &TcpSocket::requestSystemUserList);

    QObject::connect(&socket, &TcpSocket::sendSystemUserList,
                     &systemUserProcessor, &SystemUserProcessor::fillModel);


    mainWindow.setSystemUserProcessorLink(&systemUserProcessor);

    QObject::connect(&systemUserProcessor, &SystemUserProcessor::tryAddSystemUser,
                     &socket, &TcpSocket::sendAddSystemUser);

    QObject::connect(&systemUserProcessor, &SystemUserProcessor::tryEditSystemUser,
                     &socket, &TcpSocket::sendEditSystemUser);

    QObject::connect(&systemUserProcessor, &SystemUserProcessor::tryRemoveSystemUser,
                     &socket, &TcpSocket::sendRemoveSystemUser);

    QObject::connect(&socket, &TcpSocket::sendSystemUserOperationResult, &mainWindow, &MainWindow::handleSystemUserOperationResult);

    //socketThread->start();

    loginDialog.show();




    return a.exec();
}
