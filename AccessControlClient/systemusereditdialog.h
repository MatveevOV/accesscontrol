#ifndef SYSTEMUSEREDITDIALOG_H
#define SYSTEMUSEREDITDIALOG_H

#include <QDialog>
#include <QCryptographicHash>
#include <QMap>

#include "systemuser.h"

namespace Ui {
class SystemUserEditDialog;
}

class SystemUserEditDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SystemUserEditDialog(SystemUser user = SystemUser(), QWidget *parent = 0);
    ~SystemUserEditDialog();

private:
    Ui::SystemUserEditDialog *ui;
    void composePermissionWidget();
    SystemUser editedUser;

private slots:
    void checkPass();
    void on_cancelButton_released();
    void on_okButton_released();

    QList<int> formPermissionList();

signals:
    void sendUser(const SystemUser &user);

};

#endif // SYSTEMUSEREDITDIALOG_H
