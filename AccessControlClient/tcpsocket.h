#ifndef TCPSOCKET_H
#define TCPSOCKET_H

#include <QObject>
#include <QTcpSocket>
#include <QFileDialog>
#include <QFile>
#include <QDir>
#include <QMessageBox>
#include <QEvent>
#include <QDialog>
#include <QSettings>
#include <QUuid>
#include <QNetworkProxy>
#include <QTimer>
#include <QApplication>

#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>



#include "systemuser.h"




class TcpSocket: public QTcpSocket
{
    Q_OBJECT

public:
    explicit TcpSocket(QObject *parent = nullptr);

    enum Command
    {
        Login,
        DropConnect,
        GetSystemUserList,
        AddSystemUser,
        EditSystemUser,
        RemoveSystemUser
    };



public slots:
    void createSocket();
    void connectToServer(QString serverIp, int serverPort);
    void disconnectFromServer();
    void sendAuthorizationSettings(QByteArray loginData);
    void requestSystemUserList();

    void sendAddSystemUser(SystemUser user);
    void sendEditSystemUser(SystemUser user);
    void sendRemoveSystemUser(SystemUser user);

private slots:
    void slotReadyRead();
    void slotError(QAbstractSocket::SocketError err);
    void reconnectToServer();
    void sendData(Command command, QByteArray data = QByteArray());
    void processSystemUserData(QByteArray &data);
    int deserializeSystemUserOperationResult(QByteArray &data);

private:
    QTcpSocket* mTcpSocket;
    qint64     mNextBlockSize;
    int mServerPort;
    QString mServerIp;

signals:
    void disconnectDueServer();
    void systemUserAuthorized(SystemUser user);
    void systemUserAlreadyConnected(SystemUser user);
    void systemUserUnauthorized(SystemUser user);
    void sendSystemUserList(const QByteArray &usersJson);
    void sendSystemUserOperationResult(int result);

};

#endif // TCPSOCKET_H
