#ifndef SERVERSETTINGSDIALOG_H
#define SERVERSETTINGSDIALOG_H

#include <QDialog>

namespace Ui {
class ServerSettingsDialog;
}

class ServerSettingsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ServerSettingsDialog(const QString &ip, int port, QWidget *parent = 0);
    ~ServerSettingsDialog();

private slots:
    void on_cancelButton_released();

    void on_okButton_released();

signals:
    void sendServerSettings(const QString &ip, int port);


private:
    Ui::ServerSettingsDialog *ui;
};

#endif // SERVERSETTINGSDIALOG_H
