#include "logindialog.h"
#include "ui_logindialog.h"

LoginDialog::LoginDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LoginDialog)
{
    ui->setupUi(this);
    setWindowTitle("Авторизация пользователя");
    //QPixmap icon(":/images/locked.png");
    //qDebug() << icon.rect();
    //ui->iconLabel->setPixmap(icon);

    QSettings *connectionSettings = ProgramSettings::settings();

    serverIp = connectionSettings->value("ServerAddress", "127.0.0.1").toString();
    serverPort = connectionSettings->value("ServerPort", 56256).toInt();

}

LoginDialog::~LoginDialog()
{
    delete ui;
}

void LoginDialog::on_okButton_released()
{
    tryToConnect();
}

void LoginDialog::on_cancelButton_released()
{
    reject();
}

void LoginDialog::on_serverSettingsButton_released()
{
    ServerSettingsDialog *d = new ServerSettingsDialog(serverIp, serverPort, this);
    connect(d, &ServerSettingsDialog::sendServerSettings, this, [=](QString ip, int port)
    {
        serverIp = ip;
        serverPort = port;
        QSettings *connectionSettings = ProgramSettings::settings();

        connectionSettings->setValue("ServerAddress", serverIp);
        connectionSettings->setValue("ServerPort", serverPort);
    });
    d->exec();
}

bool LoginDialog::checkSystemUserPermission(const SystemUser &user)
{
    qDebug() << "permissions" << user.permissions();
    if (user.permissions().contains(SystemUser::LoginToSystem))
        return true;

    return false;

}

void LoginDialog::showAfterDisconnect()
{
    show();
    QMessageBox msgBox;
    msgBox.setText("Сервер разорвал соединение");
    msgBox.exec();

}

void LoginDialog::tryToConnect()
{
    //qDebug() << serverIp << serverPort;
    emit sendConnectionSettings(serverIp, serverPort);
}

void LoginDialog::tryToAuthorize()
{
    QByteArray passHash = QCryptographicHash::hash(ui->passEdit->text().toUtf8(), QCryptographicHash::Sha256);
    userLogin = ui->loginEdit->text();
    userPassHash = QString(passHash.toHex());
    SystemUser user(userLogin, userPassHash);
    emit sendAuthorizationSettings(user.serialize());
}

void LoginDialog::serverErrorProcessing(QAbstractSocket::SocketError socketError)
{
    qDebug() << "err" << socketError;
    QMessageBox msgBox;
    QString msgText;
    switch (socketError) {
    case QAbstractSocket::ConnectionRefusedError:
    {
        msgText = "Не удается подключиться к серверу";
    }
        break;
    case QAbstractSocket::RemoteHostClosedError:
    {
        msgText = "Сервер разорвал соединение";
        show();
    }
        break;
    default:
        break;
    }

    if (!msgText.isEmpty())
    {
        msgBox.setText(msgText);
        msgBox.exec();
    }

}

void LoginDialog::allowUserAccess(const SystemUser &user)
{
    if (checkSystemUserPermission(user))
    {
        emit needShowMainWindow(user);
        accept();
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setText("Вход в программу запрещен. Обратитесь к администратору");
        msgBox.exec();
        emit sendDisconnect();
    }
}

void LoginDialog::denyUserAccess(const SystemUser &user)
{
    QMessageBox msgBox;
    qDebug() << user.uuid() << user.getIsConnected();
    if (user.getIsConnected())
        msgBox.setText("Пользователь уже авторизован на сервере");
    else
        msgBox.setText("Неверный логин/пароль");

    msgBox.exec();
    emit sendDisconnect();

}


