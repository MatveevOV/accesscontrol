#-------------------------------------------------
#
# Project created by QtCreator 2018-03-29T14:51:16
#
#-------------------------------------------------

QT       += core gui network

CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = AccessControlClient
TEMPLATE = app

INCLUDEPATH += \
            ../Shared

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    logindialog.cpp \
    tcpsocket.cpp \
    serversettingsdialog.cpp \
    programsettings.cpp \
    systemusereditdialog.cpp \
    systemuserprocessor.cpp \
    ../Shared/systemuser.cpp


HEADERS += \
        mainwindow.h \
    logindialog.h \
    tcpsocket.h \
    serversettingsdialog.h \
    programsettings.h \
    systemusereditdialog.h \
    systemuserprocessor.h \
    ../Shared/systemuser.h


FORMS += \
        mainwindow.ui \
    logindialog.ui \
    serversettingsdialog.ui \
    systemusereditdialog.ui

RESOURCES += \
    resources.qrc


MAJOR_VER = 0
MINOR_VER = 1
PATCH_VER = 0

RC_ICONS = "appIcon.ico"
VERSION = $${MAJOR_VER}.$${MINOR_VER}.$${PATCH_VER}.$${FACTORY_VER}   # major.minor.patch
DEFINES += APP_VERSION=\\\"$$VERSION\\\"

QMAKE_TARGET_PRODUCT = AccessControlClient
QMAKE_TARGET_DESCRIPTION = AccessControlClient
QMAKE_TARGET_COPYRIGHT = Copyright
