#ifndef LOGINDIALOG_H
#define LOGINDIALOG_H

#include <QDialog>
#include <QSettings>
#include <QDebug>
#include <QAbstractSocket>
#include <QMessageBox>
#include <QCryptographicHash>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>


#include "serversettingsdialog.h"
#include "programsettings.h"
#include "systemuser.h"

namespace Ui {
class LoginDialog;
}

class LoginDialog : public QDialog
{
    Q_OBJECT

public:
    explicit LoginDialog(QWidget *parent = 0);
    ~LoginDialog();

private slots:
    void on_okButton_released();
    void on_cancelButton_released();
    void on_serverSettingsButton_released();
    bool checkSystemUserPermission(const SystemUser &user);

public slots:
    void showAfterDisconnect();
    void tryToConnect();
    void tryToAuthorize();
    void serverErrorProcessing(QAbstractSocket::SocketError socketError);
    void allowUserAccess(const SystemUser &user);
    void denyUserAccess(const SystemUser &user);

private:
    Ui::LoginDialog *ui;
    QString serverIp;
    int serverPort;
    QString userLogin;
    QString userPassHash;
    //QSettings connectionSettings;

signals:
    void sendConnectionSettings(const QString &ip, int port);
    void sendAuthorizationSettings(const QByteArray &loginData);
    void sendDisconnect();
    void needShowMainWindow(const SystemUser &user);


};

#endif // LOGINDIALOG_H
