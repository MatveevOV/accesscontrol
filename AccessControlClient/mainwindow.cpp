#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);    
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::openWindow(const SystemUser &user)
{
    this->show();
    disableTabButtons();
    fillCurrentUserInfo(user);
    checkUserPermissions(user);

    on_systemUserTabButton_released();
}
void MainWindow::on_devicesTabButton_clicked()
{
    ui->tabsStackedWidget->setCurrentIndex(0);
}

void MainWindow::on_systemUserTabButton_released()
{
    ui->tabsStackedWidget->setCurrentIndex(1);
}

void MainWindow::fillCurrentUserInfo(const SystemUser &user)
{
    currentUser = user;
    //qDebug() << currentUser.name() << currentUser.login() << currentUser.permissions();
    ui->statusBar->showMessage(QString("Пользователь: %1").arg(currentUser.name()));
}

void MainWindow::setSystemUserProcessorLink(SystemUserProcessor *systemUserProcessor)
{
    this->systemUserProcessor = systemUserProcessor;
}

void MainWindow::handleSystemUserOperationResult(int data)
{
    qDebug() << "handleSystemUserOperationResult" << data;
    QMessageBox msgBox;
    QString resultText;
    switch (data) {
    case SystemUser::UserAdded:
    {
        resultText = "Пользователь добавлен";
        systemUserProcessor->addSystemUserInModel();
    }

        break;
    case SystemUser::UserEdited:
    {
        resultText = "Пользователь измененен";
        systemUserProcessor->editSystemUserInModel();
    }

        break;
    case SystemUser::UserRemoved:
    {
        resultText = "Пользователь удален";
        systemUserProcessor->removeSystemUserInModel();
    }

        break;
    case SystemUser::UserIsDuplicate:
    {
        resultText = "Пользователь с указанным логином уже существует";

    }
        break;
    case SystemUser::UserNotFound:
    {
        resultText = "Пользователь отсутствует. Пользователь будет удален из списка";
        systemUserProcessor->removeSystemUserInModel();

    }
        break;
    default:
    {
        resultText = "Неизвестная ошибка";
    }
        break;
    }

    msgBox.setText(resultText);
    msgBox.exec();
}

void MainWindow::on_addSystemUserButton_released()
{
    SystemUserEditDialog *d = new SystemUserEditDialog();
    connect(d, &SystemUserEditDialog::sendUser, systemUserProcessor, &SystemUserProcessor::addSystemUser);
    d->exec();

}

void MainWindow::on_editSystemUserButton_released()
{
    SystemUser currentUser = systemUserProcessor->getSelectedSystemUser(ui->systemUserTable->currentIndex());
    SystemUserEditDialog *d = new SystemUserEditDialog(currentUser);
    connect(d, &SystemUserEditDialog::sendUser, systemUserProcessor, &SystemUserProcessor::editSystemUser);
    d->exec();
}

void MainWindow::on_removeSystemUserButton_released()
{
    SystemUser currentUser = systemUserProcessor->getSelectedSystemUser(ui->systemUserTable->currentIndex());
    systemUserProcessor->removeSystemUser(currentUser);

}

void MainWindow::disableTabButtons()
{
    ui->systemUserTabButton->setVisible(false);
}

void MainWindow::checkUserPermissions(const SystemUser &user)
{
    auto permissions = user.permissions();
    foreach (auto permission, permissions) {
        switch (permission) {
        case SystemUser::EditSystemUsers:
        {
            ui->systemUserTabButton->setVisible(true);
            emit needSystemUserList();
            ui->systemUserTable->setModel(systemUserProcessor->getModel());
        }

            break;
        default:
            break;
        }
    }
}

void MainWindow::serverErrorProcessing(QAbstractSocket::SocketError socketError)
{
    qDebug() << "err" << socketError;
    QMessageBox msgBox;
    QString msgText;
    switch (socketError) {
    case QAbstractSocket::RemoteHostClosedError:
    {
        //закрыть программу, если сервер разорвал содинение
        close();
    }
        break;
    default:
        break;
    }

    if (!msgText.isEmpty())
    {
        msgBox.setText(msgText);
        msgBox.exec();
    }

}

