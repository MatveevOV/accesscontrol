#ifndef PROGRAMSETTINGS_H
#define PROGRAMSETTINGS_H

#include <QObject>
#include <QSettings>

class ProgramSettings
{
public:
    ProgramSettings();

    static QSettings *settings();

private:
    static QSettings *mSettings;
};

#endif // PROGRAMSETTINGS_H
