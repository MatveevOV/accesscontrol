#include "serversettingsdialog.h"
#include "ui_serversettingsdialog.h"

ServerSettingsDialog::ServerSettingsDialog(const QString &ip, int port, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ServerSettingsDialog)
{
    ui->setupUi(this);
    ui->ipEdit->setText(ip);
    ui->portEdit->setValue(port);
}

ServerSettingsDialog::~ServerSettingsDialog()
{
    delete ui;
}

void ServerSettingsDialog::on_cancelButton_released()
{
    reject();
}

void ServerSettingsDialog::on_okButton_released()
{
    emit sendServerSettings(ui->ipEdit->text(), ui->portEdit->value());
    accept();
}
