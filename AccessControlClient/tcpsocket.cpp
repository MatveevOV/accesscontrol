#include "tcpsocket.h"

TcpSocket::TcpSocket(QObject *parent) : QTcpSocket(parent)
{

}

void TcpSocket::createSocket()
{
    mTcpSocket = new QTcpSocket(this);

    connect(mTcpSocket, &QTcpSocket::readyRead, this, &TcpSocket::slotReadyRead);
    connect(mTcpSocket, static_cast<void(QAbstractSocket::*)(QAbstractSocket::SocketError)>(&QAbstractSocket::error),
            this, &TcpSocket::slotError);


    mNextBlockSize = 0;
}

void TcpSocket::connectToServer(QString serverIp, int serverPort)
{

    mServerIp = serverIp;
    mServerPort= serverPort;
    qDebug() << "connect to" << mServerIp << mServerPort;

    mTcpSocket->connectToHost(mServerIp,mServerPort);


    if (mTcpSocket->waitForConnected(1000))
        emit connected();
    else
        emit error(QAbstractSocket::ConnectionRefusedError);




}

void TcpSocket::disconnectFromServer()
{
    mTcpSocket->disconnectFromHost();
    if (mTcpSocket->state() == QAbstractSocket::UnconnectedState ||
        mTcpSocket->waitForDisconnected(1000))
        qDebug("Disconnected!");
    //qDebug() << mTcpSocket->reset();
}

void TcpSocket::sendAuthorizationSettings(QByteArray loginData)
{
    sendData(Login, loginData);
}

void TcpSocket::requestSystemUserList()
{
    sendData(GetSystemUserList);
}

void TcpSocket::sendAddSystemUser(SystemUser user)
{
    sendData(AddSystemUser, user.serialize());
}

void TcpSocket::sendEditSystemUser(SystemUser user)
{
    sendData(EditSystemUser, user.serialize());
}

void TcpSocket::sendRemoveSystemUser(SystemUser user)
{
    sendData(RemoveSystemUser, user.serialize());
}

void TcpSocket::slotReadyRead()
{
    QDataStream in(mTcpSocket);
    qDebug() << "client ready read" << mNextBlockSize << mTcpSocket->bytesAvailable();
    in.setVersion(QDataStream::Qt_5_5);
    for (;;)
    {
        if (mNextBlockSize == 0) {
            if (mTcpSocket->bytesAvailable() < sizeof(quint64)) {
                break;
            }
            in >> mNextBlockSize;
        }

        if (mTcpSocket->bytesAvailable() < mNextBlockSize) {
            break;
        }


        quint32 request;
        in >> request;

        switch (request) {
        case Login:
        {
            QByteArray data = mTcpSocket->readAll();
            qDebug() << "login receive"/* << data*/;
            processSystemUserData(data);
            mNextBlockSize = 0;
            return;
        }
        case DropConnect:
        {
            mTcpSocket->readAll();
            disconnectFromServer();
            emit disconnectDueServer();
            mNextBlockSize = 0;
            return;
        }
            break;
        case GetSystemUserList:
        {
            QByteArray data = mTcpSocket->readAll();
            qDebug() << "receive system user list"/* << data*/;
            emit sendSystemUserList(data);
            mNextBlockSize = 0;
            return;
        }
            break;

        case AddSystemUser:
        {
            QByteArray data = mTcpSocket->readAll();
            qDebug() << "add user request"/* << data*/;
            emit sendSystemUserOperationResult(deserializeSystemUserOperationResult(data));
            mNextBlockSize = 0;
            return;
        }
            break;

        case EditSystemUser:
        {
            QByteArray data = mTcpSocket->readAll();
            qDebug() << "edit user request"/* << data*/;
            emit sendSystemUserOperationResult(deserializeSystemUserOperationResult(data));
            mNextBlockSize = 0;
            return;
        }
            break;

        case RemoveSystemUser:
        {
            QByteArray data = mTcpSocket->readAll();
            qDebug() << "remove user request"/* << data*/;
            emit sendSystemUserOperationResult(deserializeSystemUserOperationResult(data));
            mNextBlockSize = 0;
            return;
        }
            break;
        default:
            break;
        }


    }

}

void TcpSocket::sendData(Command command, QByteArray data)
{

    if (mTcpSocket->state() == QAbstractSocket::ConnectedState)
    {
        int blockFileSize = 1024;

        QByteArray bArray;
        QDataStream dStream(&bArray, QIODevice::WriteOnly);
        dStream.setVersion(QDataStream::Qt_5_5);

        quint64 dataSize = data.size();

        dStream << quint64(sizeof(quint32) + dataSize);

        dStream << quint32(command);

        mTcpSocket->write(bArray);

        mTcpSocket->waitForBytesWritten();

        quint64 bytesDone = 0;

        while(bytesDone < dataSize)
        {
            QByteArray ba = data.mid(bytesDone, blockFileSize);
            mTcpSocket->write(ba);
            bytesDone += blockFileSize;
            //qDebug() << "send part" << ba;
            mTcpSocket->waitForBytesWritten();
        }

    }

}

void TcpSocket::processSystemUserData(QByteArray &data)
{

    qDebug() << data;
    SystemUser user(data);
    qDebug() << user.uuid() << user.getIsConnected();
    if (user.getIsConnected())
    {
        emit systemUserAlreadyConnected(user);
    }
    else
    {
        if (user.uuid().isNull())
        {
            emit systemUserUnauthorized(user);
        }
        else
        {
            emit systemUserAuthorized(user);

        }
    }


}

int TcpSocket::deserializeSystemUserOperationResult(QByteArray &data)
{
    int result = -1;
    QJsonDocument doc = QJsonDocument::fromJson(data);
    if (doc.isObject())
    {
        result = doc.object().value("Result").toInt();
    }
    qDebug() << "deserializeSystemUserOperationResult" << result << data;

    return result;
}

void TcpSocket::slotError(QAbstractSocket::SocketError err)
{
       qDebug() << err;
       mTcpSocket->close();
       emit error(err);
}

void TcpSocket::reconnectToServer()
{
    if (mTcpSocket->state() == QAbstractSocket::UnconnectedState)
    {
        mTcpSocket->connectToHost(mServerIp,mServerPort);

    }
}
