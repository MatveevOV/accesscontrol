#include "systemuserprocessor.h"

SystemUserProcessor::SystemUserProcessor()
{
    model = new QStandardItemModel;
    QStringList systemUserModelHeaders;
    systemUserModelHeaders << "Имя" << "Логин";
    model->setHorizontalHeaderLabels(systemUserModelHeaders);
}

QStandardItemModel *SystemUserProcessor::getModel() const
{
    return model;
}

void SystemUserProcessor::addSystemUser(const SystemUser &user)
{
    newUser = user;
    QUuid uuid = QUuid::createUuid();
    newUser.setUuid(uuid);

    emit tryAddSystemUser(newUser);
}

void SystemUserProcessor::editSystemUser(const SystemUser &user)
{
    QUuid uuid = model->data(selectedUserIndex,Qt::UserRole).value<QUuid>();
    newUser = user;
    newUser.setUuid(uuid);
    emit tryEditSystemUser(newUser);
}

void SystemUserProcessor::removeSystemUser(const SystemUser &user)
{
    emit tryRemoveSystemUser(user);
}

void SystemUserProcessor::addSystemUserInModel()
{
    QStandardItem *name = new QStandardItem(newUser.name());
    name->setData(newUser.uuid(),Qt::UserRole);
    QStandardItem *login = new QStandardItem(newUser.login());

    QList<QStandardItem*> list;
    list << name << login;
    this->users.insert(newUser.uuid(), newUser);
    model->appendRow(list);
}

void SystemUserProcessor::editSystemUserInModel()
{
    QUuid uuid = model->data(selectedUserIndex,Qt::UserRole).value<QUuid>();

    QStandardItem *name = model->item(selectedUserIndex.row(),0);
    name->setData(newUser.name(),Qt::DisplayRole);
    QStandardItem *login = model->item(selectedUserIndex.row(),1);
    login->setData(newUser.login(),Qt::DisplayRole);

    this->users.insert(uuid, newUser);
}

void SystemUserProcessor::removeSystemUserInModel()
{
    QUuid uuid = model->data(selectedUserIndex,Qt::UserRole).value<QUuid>();
    if (users.contains(uuid))
        users.remove(uuid);

    model->removeRow(selectedUserIndex.row());
}

void SystemUserProcessor::setSelectedUserIndex(const QModelIndex &value)
{
    selectedUserIndex = value;
}

SystemUser SystemUserProcessor::getSelectedSystemUser(const QModelIndex &value)
{
    QModelIndex firstColumnIndex = value.sibling(value.row(),0);
    if (firstColumnIndex.isValid())
    {
        setSelectedUserIndex(firstColumnIndex);
        QUuid uuid = model->data(firstColumnIndex,Qt::UserRole).value<QUuid>();
        if (users.contains(uuid))
            return users.value(uuid);
    }
    return SystemUser();
}

void SystemUserProcessor::fillModel(const QByteArray &usersJson)
{
    qDebug() << "fill model";
    QList<SystemUser> users = SystemUser::deserializeList(usersJson);

    foreach (auto user, users) {
        //qDebug() << user.uuid();
        QStandardItem *name = new QStandardItem(user.name());
        name->setData(user.uuid(),Qt::UserRole);
        QStandardItem *login = new QStandardItem(user.login());

        QList<QStandardItem*> list;
        list << name << login;
        this->users.insert(user.uuid(), user);
        model->appendRow(list);

    }
}
