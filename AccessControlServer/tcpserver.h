#ifndef TCPSERVER_H
#define TCPSERVER_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QMessageBox>
#include <QNetworkInterface>
#include <QFile>
#include <QDataStream>
#include <QDir>
#include <QSettings>
#include <QHostInfo>
#include <QUuid>
#include <QStandardItem>
#include <QElapsedTimer>
#include <QTimer>
#include <QDebug>
#include <QElapsedTimer>

#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>

#include "databasehelper.h"
#include "../Shared/systemuser.h"

class TcpServer : public QObject
{
    Q_OBJECT

public:
    explicit TcpServer(DatabaseHelper *dbHelper, QObject *parent = nullptr);

    enum Command
    {
        Login,
        DropConnect,
        GetSystemUserList,
        AddSystemUser,
        EditSystemUser,
        RemoveSystemUser
    };

private slots:
    void clientConnectProcessing();
    void clientDisconnectProcessing();
    void readFromClient();
    QByteArray authorizeClient(const QString &login, const QString &pass);
    QByteArray serializeSystemUserOperationResult(int result);
    QByteArray readData(QTcpSocket * clientSocket);
    void sendData(Command command, const QByteArray &data, QTcpSocket *clientSocket);

private:
    QTcpServer* m_ptcpServer;
    qint64 mNextBlockSize = 0;
    QString m_Ip;
    DatabaseHelper *dbHelper;
    QHash<QTcpSocket * , QString> connectedClients;

signals:
    void clientConnected();
    void clientDisconnected();
    void sendConnectedUser(const QString &login, const QString &ip);
    void sendDisconnectedUser(const QString &login);

public slots:
    void dropClients();

};

#endif // TCPSERVER_H
