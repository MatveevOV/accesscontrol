#ifndef DATABASEHELPER_H
#define DATABASEHELPER_H

#include <QObject>
#include <QSql>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlDatabase>
#include <QFile>
#include <QDate>
#include <QUuid>
#include <QDebug>
#include <QPoint>
#include <QElapsedTimer>

#include "systemuser.h"

/* Директивы имен таблицы, полей таблицы и базы данных */
#define DATABASE_NAME               "AccessControl.db"

#define SYSTEM_USERS                    "SystemUsers"         // Название таблицы
#define SYSTEM_USER_UID                 "Uid"
#define SYSTEM_USER_NAME                "Name"
#define SYSTEM_USER_LOGIN               "Login"
#define SYSTEM_USER_PASSHASH            "PassHash"


#define SYSTEM_USERS_PERMISSION         "SystemUsersPermissions"         // Название таблицы
#define PERMISSIONS_USER_UID            "UserUid"
#define PERMISSION                      "Permission"

class DatabaseHelper: public QObject
{
    Q_OBJECT

public:
    explicit DatabaseHelper(QObject *parent = 0);
    ~DatabaseHelper();

    void connectToDataBase(const QString &pathToDb);

private:
    QSqlDatabase db;
    bool openDataBase(const QString &pathToDb);
    bool restoreDataBase(const QString &pathToDb);
    bool createTables();
    void closeDataBase();
    bool createSystemUsersTable();
    bool createSystemUsersPermissionsTable();

public slots:
    //добавить нового пользователя
    int insertUser(const SystemUser &user);
    int updateUser(const SystemUser &user);
    int deleteUser(const SystemUser &user);
    SystemUser getSystemUser(const QString &login, const QString &passHash) const;
    QList<SystemUser> getSystemUsers() const;

    QList<int> getSystemUserPermissions(const QString &uid) const;

};

#endif // DATABASEHELPER_H
