#include "databasehelper.h"

DatabaseHelper::DatabaseHelper(QObject *parent) : QObject(parent)
{

}

DatabaseHelper::~DatabaseHelper()
{

}

void DatabaseHelper::connectToDataBase(const QString &pathToDb)
{
    qDebug() << QString(pathToDb + "/" +  DATABASE_NAME);
    if(!QFile(QString(pathToDb + "/" +  DATABASE_NAME)).exists()){
        this->restoreDataBase(pathToDb);
    } else {
        this->openDataBase(pathToDb);
    }
}

bool DatabaseHelper::openDataBase(const QString &pathToDb)
{
    /* База данных открывается по заданному пути
     * и имени базы данных, если она существует
     * */
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(QString(pathToDb + "/" +  DATABASE_NAME));
    if(db.open()){
        return true;
    } else {
        return false;
    }
}

bool DatabaseHelper::restoreDataBase(const QString &pathToDb)
{
    // Если база данных открылась ...
    if(this->openDataBase(pathToDb)){
        // Производим восстановление базы данных
        return (this->createTables()) ? true : false;
    } else {
        qDebug() << "Не удалось восстановить базу данных";
        return false;
    }
    return false;
}

bool DatabaseHelper::createTables()
{
    bool result = createSystemUsersTable() && createSystemUsersPermissionsTable();

    QList<int> admPermissions;
    admPermissions << 1 << 2;
    insertUser(SystemUser(QUuid::createUuid(),
                          "adm",
                          "Администратор",
                          "6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b",
                          admPermissions));

    QList<int> operPermissions;
    operPermissions << 1;
    insertUser(SystemUser(QUuid::createUuid(),                          
                          "oper",
                          "Оператор",
                          "6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b",
                          operPermissions));

    for (int i = 0; i < 100; i++)
    {
        db.transaction();
        for (int j = 0; j < 1000; j++)
            insertUser(SystemUser(QUuid::createUuid(),
                              QString::number(i*1000 + j),
                              "Оператор",
                              "6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b",
                              operPermissions));

        db.commit();
    }

    return result;
}

void DatabaseHelper::closeDataBase()
{
    db.close();
}

bool DatabaseHelper::createSystemUsersTable()
{
    QSqlQuery query;
    if(!query.exec( "CREATE TABLE IF NOT EXISTS " SYSTEM_USERS " ("
                            SYSTEM_USER_UID       " TEXT    NOT NULL,"
                            SYSTEM_USER_NAME      " TEXT    NOT NULL,"
                            SYSTEM_USER_LOGIN     " TEXT    NOT NULL,"
                            SYSTEM_USER_PASSHASH  " TEXT    NOT NULL)"
                    )){
        qDebug() << "DataBase: error of create " << SYSTEM_USERS;
        qDebug() << query.lastError().text();
        return false;
    } else {
        qDebug() << SYSTEM_USERS << "created";
        return true;
    }
    return false;
}

bool DatabaseHelper::createSystemUsersPermissionsTable()
{
    QSqlQuery query;
    if(!query.exec( "CREATE TABLE IF NOT EXISTS " SYSTEM_USERS_PERMISSION " ("
                            PERMISSIONS_USER_UID       " TEXT    NOT NULL,"
                            PERMISSION                 " INTEGER    NOT NULL)"
                    )){
        qDebug() << "DataBase: error of create " << SYSTEM_USERS_PERMISSION;
        qDebug() << query.lastError().text();
        return false;
    } else {
        qDebug() << SYSTEM_USERS_PERMISSION << "created";
        return true;
    }
    return false;
}

int DatabaseHelper::insertUser(const SystemUser &user)
{
    QSqlQuery query;
    //qDebug() << user.name();

    QString selectUserUid = QString("SELECT " SYSTEM_USER_UID " FROM " SYSTEM_USERS " WHERE " SYSTEM_USER_LOGIN " = '%1'")
            .arg(user.login());



    QString userId;
    /*
    bool selectBool = query.exec(selectUserUid);
    //qDebug() << selectBool;
    if(selectBool)
    {
        query.next();
        if (query.isValid()){
            //пользователь с таким логином уже существует
            return SystemUser::UserIsDuplicate;
        }

    }
    */
    if(userId.isEmpty())
    {
        userId = user.uuid().toString();
        //qDebug() << "createUser" << userId;

        query.prepare("INSERT INTO " SYSTEM_USERS " ( " SYSTEM_USER_UID ", "
                                                 SYSTEM_USER_NAME ", "
                                                 SYSTEM_USER_LOGIN ", "
                                                 SYSTEM_USER_PASSHASH " ) "
                      "VALUES (:Uid, :Name, :Login, :PassHash)");
        query.bindValue(":Uid",userId);
        query.bindValue(":Name", user.name());
        query.bindValue(":Login", user.login());
        query.bindValue(":PassHash", user.passHash());

        qDebug() << "insert new user" << user.login() << query.exec() << query.lastError();

        for (int i = 0; i < user.permissions().length(); i++)
        {
            query.prepare("INSERT INTO " SYSTEM_USERS_PERMISSION " ( " PERMISSIONS_USER_UID ", "
                                                     PERMISSION " ) "
                          "VALUES (:UserUid, :Permission)");
            query.bindValue(":UserUid",userId);
            query.bindValue(":Permission", user.permissions().at(i));

            qDebug() << "insert user permission" << query.exec() << query.lastError();
        }

        return SystemUser::UserAdded;

    }
    return SystemUser::UnknownError;
}

int DatabaseHelper::updateUser(const SystemUser &user)
{
    QSqlQuery query;

    //удалить старые права

    QString deletePermissions =
            QString("DELETE FROM " SYSTEM_USERS_PERMISSION " WHERE " PERMISSIONS_USER_UID " = '%1'").arg(user.uuid().toString());
    qDebug() << "remove last permissionns" << query.exec(deletePermissions) << query.lastError();

    //заполнить права доступа

    for (int i = 0; i < user.permissions().length(); i++)
    {
        query.prepare("INSERT INTO " SYSTEM_USERS_PERMISSION " ( " PERMISSIONS_USER_UID ", "
                      PERMISSION " ) "
                                 "VALUES (:UserUid, :Permission)");
        query.bindValue(":UserUid",user.uuid().toString());
        query.bindValue(":Permission", user.permissions().at(i));

        qDebug() << "insert user permission" << query.exec() << query.lastError();
    }
    QString updatedUser;
    if (user.passHash().isEmpty())
    {
        updatedUser =
                QString("UPDATE " SYSTEM_USERS " SET " SYSTEM_USER_NAME " = '%1', "
                                                       SYSTEM_USER_LOGIN " = '%2' "
                                          " WHERE " SYSTEM_USER_UID " = '%4'").arg(user.name())
                                                                              .arg(user.login())
                                                                              .arg(user.uuid().toString());
    }
    else
    {
        updatedUser =
                QString("UPDATE " SYSTEM_USERS " SET " SYSTEM_USER_NAME " = '%1', "
                                                       SYSTEM_USER_LOGIN " = '%2', "
                                                       SYSTEM_USER_PASSHASH " = '%3'"
                                             " WHERE " SYSTEM_USER_UID " = '%4'").arg(user.name())
                                                                                 .arg(user.login())
                                                                                 .arg(user.passHash())
                                                                                 .arg(user.uuid().toString());
    }
    qDebug() << "update user" << updatedUser << query.exec(updatedUser) << query.lastError();

    qDebug() << "rows updated" << query.numRowsAffected();

    if (query.numRowsAffected() == 1)
        return SystemUser::UserEdited;

    if (query.numRowsAffected() == 0)
        return SystemUser::UserNotFound;

    return SystemUser::UnknownError;
}

int DatabaseHelper::deleteUser(const SystemUser &user)
{
    QSqlQuery query;

    QString deletePermissions =
            QString("DELETE FROM " SYSTEM_USERS_PERMISSION " WHERE " PERMISSIONS_USER_UID " = '%1'").arg(user.uuid().toString());
    qDebug() << "remove permissions" << query.exec(deletePermissions) << query.lastError();

    QString deleteUser =
            QString("DELETE FROM " SYSTEM_USERS " WHERE " SYSTEM_USER_UID " = '%1'").arg(user.uuid().toString());
    qDebug() << "remove user" << query.exec(deleteUser) << query.lastError();

    qDebug() << "rows removed" << query.numRowsAffected();

    if (query.numRowsAffected() == 1)
        return SystemUser::UserRemoved;

    if (query.numRowsAffected() == 0)
        return SystemUser::UserNotFound;

    return SystemUser::UnknownError;

}

SystemUser DatabaseHelper::getSystemUser(const QString &login, const QString &passHash) const
{
    QSqlQuery query;
    SystemUser user;
    QString selectUserUid = QString("SELECT " SYSTEM_USER_UID ", " SYSTEM_USER_NAME " FROM "  SYSTEM_USERS " WHERE "
                                SYSTEM_USER_LOGIN " = '%1' AND "
                                SYSTEM_USER_PASSHASH " = '%2'").arg(login).arg(passHash);

    qDebug() << login << passHash;
    bool result = query.exec(selectUserUid);
    qDebug() << result << selectUserUid;
    if(result)
    {
        query.next();
        qDebug() << query.value(0).toString() << query.value(1).toString();
        user.setUuid(query.value(0).toString());
        user.setName(query.value(1).toString());
        //user.setLogin(query.value(2).toString());

        if (!user.uuid().isNull())
        {
            QList<int> permissions = getSystemUserPermissions(user.uuid().toString());
            user.setPermissions(permissions);
        }
    }


    return user;
}

QList<SystemUser> DatabaseHelper::getSystemUsers() const
{
    QList<SystemUser> users;

    QSqlQuery query;

    QString selectPermissions = QString("SELECT " SYSTEM_USERS "." SYSTEM_USER_UID ", "
                                        SYSTEM_USERS "." SYSTEM_USER_NAME ", "
                                        SYSTEM_USERS "." SYSTEM_USER_LOGIN ", "
                                        SYSTEM_USERS_PERMISSION "." PERMISSION
                                        " FROM " SYSTEM_USERS
                                        " LEFT JOIN " SYSTEM_USERS_PERMISSION
                                        " ON " SYSTEM_USERS "." SYSTEM_USER_UID " = "
                                        SYSTEM_USERS_PERMISSION "." PERMISSIONS_USER_UID
                                        " ORDER BY " SYSTEM_USERS "." SYSTEM_USER_LOGIN " ASC");
    if (query.exec(selectPermissions))
    {
        QElapsedTimer timer;
        timer.start();
        QString prevLogin = "";
        SystemUser user;
        while (query.next())
        {
            QUuid uuid = QUuid(query.value(0).toString());
            //qDebug() << uuid;
            QString name = query.value(1).toString();
            QString login = query.value(2).toString();
            int permission = query.value(3).toInt();

            //заполнить самого первого
            if (prevLogin.isEmpty())
            {
                user.setUuid(uuid);
                user.setLogin(login);
                user.setName(name);
                user.addPermission(permission);
                prevLogin = login;
                continue;

            }
            //продолжить заполнять
            if (prevLogin == login)
            {
                user.addPermission(permission);

            }
            //зафиксировать пользователя, начать заполнять нового
            else
            {
                users << user;
                user.clearPermissions();
                user.setUuid(uuid);
                user.setLogin(login);
                user.setName(name);
                user.addPermission(permission);

                prevLogin = login;
            }

        }
        //добавить последнего пользователя
        users << user;
        qDebug() << users.length();
        qDebug() << timer.elapsed();
        return users;
    }
    else{
        qDebug() << "nothing" << query.lastError();
        return QList<SystemUser>();
    }

    return users;
}

QList<int> DatabaseHelper::getSystemUserPermissions(const QString &uid) const
{
    QSqlQuery query;

    QString selectPermissions = QString("SELECT " PERMISSION " FROM " SYSTEM_USERS_PERMISSION
                               " WHERE " PERMISSIONS_USER_UID " = '%1'").arg(uid);
    if (query.exec(selectPermissions))
    {
        QList<int> data;
        while (query.next())
        {
            int permission = query.value(0).toInt();
            data.append(permission);
        }
        return data;
    }
    else{
        qDebug() << "nothing" << query.lastError();

        return QList<int>();
    }
}
