#include "mainwindow.h"
#include <QApplication>
#include <QMessageBox>
#include <QObject>
#include "tcpserver.h"
#include "databasehelper.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    if (!QSystemTrayIcon::isSystemTrayAvailable()) {
        QMessageBox::critical(0, QObject::tr("Systray"),
                              QObject::tr("I couldn't detect any system tray "
                                          "on this system."));
        return 1;
    }
    QApplication::setQuitOnLastWindowClosed(false);

    MainWindow window;
    DatabaseHelper dbHelper;
    dbHelper.connectToDataBase("./");

    TcpServer server(&dbHelper);

    QObject::connect(&server, &TcpServer::sendConnectedUser, &window, &MainWindow::addConnectedClient);
    QObject::connect(&server, &TcpServer::sendDisconnectedUser, &window, &MainWindow::removeConnectedClient);

    QObject::connect(&window, &MainWindow::needDropClients, &server, &TcpServer::dropClients);

    return a.exec();
}
