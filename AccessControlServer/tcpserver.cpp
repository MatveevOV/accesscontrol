#include "tcpserver.h"

TcpServer::TcpServer(DatabaseHelper *dbHelper, QObject *parent) : QObject(parent)
{
    m_ptcpServer = new QTcpServer(this);
    if (!m_ptcpServer->listen(QHostAddress::Any, 56256))
    {
        QMessageBox::critical(0,
                              "Server Error",
                              "Unable to start the server:"
                              + m_ptcpServer->errorString()
                             );
        m_ptcpServer->close();
        return;
    }
    else
    {

        QString ipAddress;
        QList<QHostAddress> ipAddressesList = QNetworkInterface::allAddresses();
        // use the first non-localhost IPv4 address
        for (int i = 0; i < ipAddressesList.size(); ++i) {
            if (ipAddressesList.at(i) != QHostAddress::LocalHost &&
                ipAddressesList.at(i).toIPv4Address()) {
                ipAddress = ipAddressesList.at(i).toString();
                break;
            }
        }
        // if we did not find one, use IPv4 localhost
        if (ipAddress.isEmpty())
            ipAddress = QHostAddress(QHostAddress::LocalHost).toString();

        m_Ip = ipAddress;

        qDebug() << QString("server address: %1 port: %2").arg(ipAddress).arg(m_ptcpServer->serverPort());


    }

    connect(m_ptcpServer, &QTcpServer::newConnection, this, &TcpServer::clientConnectProcessing);

    this->dbHelper = dbHelper;


}

void TcpServer::clientConnectProcessing()
{
    QTcpSocket* pClientSocket = m_ptcpServer->nextPendingConnection();
    qDebug() << "new connection" << pClientSocket <<  pClientSocket->peerAddress().toString() << pClientSocket->socketDescriptor();
    emit clientConnected();
    connect(pClientSocket, &QTcpSocket::disconnected, this, &TcpServer::clientDisconnectProcessing);
    connect(pClientSocket, &QTcpSocket::readyRead, this, &TcpServer::readFromClient);


}

void TcpServer::clientDisconnectProcessing()
{
    qDebug() << "client disconnected";
    QTcpSocket* pClient = static_cast<QTcpSocket*>(QObject::sender());
    emit sendDisconnectedUser(connectedClients.value(pClient));
    connectedClients.remove(pClient);
    emit clientDisconnected();

}

void TcpServer::readFromClient()
{
    QTcpSocket* pClientSocket = (QTcpSocket*)sender();
    //qDebug() << "read socket" << pClientSocket << pClientSocket->bytesAvailable();
    QDataStream in(pClientSocket);
    in.setVersion(QDataStream::Qt_5_5);
    for (;;)
    {
        //qDebug() << m_nNextBlockSize <<mTcpSocket->bytesAvailable();
        if (mNextBlockSize == 0) {
            if (pClientSocket->bytesAvailable() < sizeof(quint64)) {
                break;
            }
            in >> mNextBlockSize;
        }

        if (pClientSocket->bytesAvailable() < mNextBlockSize) {
            break;
        }
        //qDebug() << "bytes received" << pClientSocket->bytesAvailable();

        quint32 request;
        in >> request;

        switch (request)
        {
        case Login:
        {
            qDebug() << "login receive";
            QByteArray data = pClientSocket->readAll();
            SystemUser clientUser = SystemUser(data);

            auto iter = connectedClients.begin();
            bool userConnected = false;
            for (; iter != connectedClients.end(); ++iter)
            {
                if (iter.value() == clientUser.login())
                {
                    userConnected = true;
                    break;
                }
            }
            qDebug() << userConnected;
            if (!userConnected)
            {
                QByteArray response = authorizeClient(clientUser.login(), clientUser.passHash());
                connectedClients.insert(pClientSocket, clientUser.login());
                emit sendConnectedUser(clientUser.login(), pClientSocket->localAddress().toString());
                sendData(Login, response, pClientSocket);
            }
            else
            {
                clientUser.setIsConnected(true);
                sendData(Login, clientUser.serialize(), pClientSocket);
            }

            mNextBlockSize = 0;
            return;

        }
        case GetSystemUserList:
        {
            qDebug() << "request for system users";
            QElapsedTimer timer;
            timer.start();
            QByteArray data = SystemUser::serializeList(dbHelper->getSystemUsers());
            qDebug() << timer.elapsed();
            sendData(GetSystemUserList, data, pClientSocket);

            mNextBlockSize = 0;
            return;

        }
            break;
        case AddSystemUser:
        {
            qDebug() << "request for system user insert";
            QByteArray data = pClientSocket->readAll();
            QByteArray request = serializeSystemUserOperationResult(dbHelper->insertUser(SystemUser(data)));
            sendData(AddSystemUser, request, pClientSocket);
            mNextBlockSize = 0;
            return;
        }
            break;
        case EditSystemUser:
        {
            qDebug() << "request for system user update";
            QByteArray data = pClientSocket->readAll();
            QByteArray request = serializeSystemUserOperationResult(dbHelper->updateUser(SystemUser(data)));
            sendData(EditSystemUser, request, pClientSocket);
            mNextBlockSize = 0;
            return;
        }
            break;
        case RemoveSystemUser:
        {
            qDebug() << "request for system user remove";
            QByteArray data = pClientSocket->readAll();
            QByteArray request = serializeSystemUserOperationResult(dbHelper->deleteUser(SystemUser(data)));
            sendData(RemoveSystemUser, request, pClientSocket);
            mNextBlockSize = 0;
            return;
        }
            break;
        default:
            break;
        }
   }

}


QByteArray TcpServer::authorizeClient(const QString &login, const QString &pass)
{
    QByteArray response;
    //запрос из бд логина/пароля
    SystemUser user = dbHelper->getSystemUser(login, pass);
    qDebug() << "user uuid" << user.uuid();
    response = user.serialize();
    return response;
}

QByteArray TcpServer::serializeSystemUserOperationResult(int result)
{
    QJsonObject jsonResult;
    jsonResult["Result"] = result;
    QJsonDocument document(jsonResult);
    return document.toJson();
}

QByteArray TcpServer::readData(QTcpSocket *clientSocket)
{
    QDataStream in(clientSocket);
    in.setVersion(QDataStream::Qt_5_5);

    quint64 length = clientSocket->bytesAvailable();

    quint64 lBytes = 0;

    QByteArray data;


    while (lBytes < length)
    {
        data.append(clientSocket->readAll());
        lBytes += data.size();

    }

    return data;
}

void TcpServer::sendData(Command command, const QByteArray &data, QTcpSocket *clientSocket)
{

    if (clientSocket->state() == QAbstractSocket::ConnectedState)
    {
        int blockFileSize = 1024;

        QByteArray bArray;
        QDataStream dStream(&bArray, QIODevice::WriteOnly);
        dStream.setVersion(QDataStream::Qt_5_5);

        quint64 dataSize = data.size();

        dStream << quint64(sizeof(quint32) + dataSize);

        dStream << quint32(command);

        clientSocket->write(bArray);

        clientSocket->waitForBytesWritten();

        quint64 bytesDone = 0;

        while(bytesDone < dataSize)
        {
            QByteArray ba = data.mid(bytesDone, blockFileSize);
            clientSocket->write(ba);
            bytesDone += blockFileSize;
            //qDebug() << "send part" << ba;
            clientSocket->waitForBytesWritten();
        }

    }

}

void TcpServer::dropClients()
{
    auto iter = connectedClients.begin();
    for (; iter != connectedClients.end(); ++iter)
    {
        sendData(DropConnect,QByteArray(),iter.key());
    }
}

