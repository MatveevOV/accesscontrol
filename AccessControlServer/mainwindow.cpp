#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    createActions();
    createTrayIcon();

    trayIcon->show();

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::createActions()
{


    maximizeAction = new QAction(tr("Показать"), this);
    connect(maximizeAction, &QAction::triggered, this, &QWidget::show);


    quitAction = new QAction(tr("Выход"), this);
    connect(quitAction, &QAction::triggered, qApp, &QCoreApplication::quit);
}

void MainWindow::createTrayIcon()
{
    trayIconMenu = new QMenu(this);
    trayIconMenu->addAction(maximizeAction);
    trayIconMenu->addAction(quitAction);

    trayIcon = new QSystemTrayIcon(this);
    trayIcon->setContextMenu(trayIconMenu);
    trayIcon->setIcon(QPixmap(":/appIcon.ico"));

    trayIcon->setToolTip("Сервер приложений AccessControl");
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    if (trayIcon->isVisible())
    {
        hide();
        event->ignore();
    }
}

void MainWindow::addConnectedClient(const QString &login, const QString &ip)
{
    QTableWidgetItem *loginItem = new QTableWidgetItem(login);
    QTableWidgetItem *ipItem = new QTableWidgetItem(ip);

    int row = ui->connectedClientTable->rowCount();
    ui->connectedClientTable->setRowCount(row + 1);
    ui->connectedClientTable->setItem(row, 0, ipItem);
    ui->connectedClientTable->setItem(row, 1, loginItem);


}

void MainWindow::removeConnectedClient(const QString &login)
{
    auto itemList = ui->connectedClientTable->findItems(login, Qt::MatchExactly);
    if (itemList.length() == 1)
        ui->connectedClientTable->removeRow(itemList.at(0)->row());

}

void MainWindow::on_dropClientsButton_released()
{
    emit needDropClients();
}
