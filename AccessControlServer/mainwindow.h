#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSystemTrayIcon>
#include <QCloseEvent>
#include <QTableWidget>
#include <QTableWidgetItem>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void closeEvent(QCloseEvent *event);

public slots:

    void addConnectedClient(const QString &login, const QString &ip);
    void removeConnectedClient(const QString &login);

private slots:
    void on_dropClientsButton_released();

private:
    Ui::MainWindow *ui;

    QSystemTrayIcon *trayIcon;
    QMenu *trayIconMenu;
    QAction *maximizeAction;
    QAction *quitAction;

    int clientsCount = 0;

    void createTrayIcon();
    void createActions();

signals:
    void needDropClients();
};

#endif // MAINWINDOW_H
